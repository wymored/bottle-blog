#!/usr/bin/env python3
import os
import sys
from bottle import route, run, template, static_file, request,redirect,error , response
import sqlite3 # database
import datetime #to manipulate date
from slugify import slugify #to generate safe urls for posts
from passlib.hash import bcrypt #to encript/check passwords


#Config section
secret_key_cookie = 'changeitplease'
username = request.get_cookie("user", secret=secret_key_cookie)
sitename = "Bottle Blog"
pagetitle = "Home"
section = "Blog"
message = ''
message = username
# Warning: This file is created in the current directory
con = sqlite3.connect('blog.db',isolation_level=None)


#prepare section


con.row_factory = sqlite3.Row
db = con.cursor()

def create_table_page():
    db.execute("""CREATE TABLE post (
    id INTEGER PRIMARY KEY,
    title char(256) NOT NULL,
    slug char(256) NOT NULL,
    content TEXT NOT NULL,
    html_content TEXT NOT NULL,
    posted DATE NOT NULL,
    active bool NOT NULL)
    """)
    con.commit()

def create_table_user():
    db.execute("""CREATE TABLE user (
    id INTEGER PRIMARY KEY,
    name char(256) NOT NULL,
    email char(256) NOT NULL,
    passw TEXT NOT NULL,
    created DATE NOT NULL,
    active bool NOT NULL)
    """)
    con.commit()
#create_table_user()
def create_user():
    # Calculating a hash for password
    passwhash = bcrypt.encrypt('changeit', rounds=12)

    db.execute("""INSERT INTO
    user (name,email,passw,created, active)
    values ('admin', 'admin@mysite.com', '%s', '%s', 1 )""" % (passwhash,datetime.datetime.today().isoformat()))
    con.commit()

#create_user()


def change_admin(name,email,passw):
    # Calculating a hash for password
    passwhash = bcrypt.encrypt(passw, rounds=12)

    db.execute("""update
    user set name= ?,email=?, passw = ?
    where id = 1 """ , (name,email,passwhash))
    con.commit()
    print("admin account changed")
#change_admin('name','newemail@newadmin.com','newpasswd')





## util function section
# fix the links in subdirectories pages, only for internal links
def ilink(path):
    l = "http://"+request['HTTP_HOST']+"/"+path
    return l


@error(404)
def error404(error):
    return template('base.html', globals(),pagetitle='Error 404', basetext='Error 404: This page was not found')




##pages section
@route('/about')
def about():
    return template('base.html', globals(), pagetitle='About', basetext="""
#About
Simple Blog is a project that aim to
be used in new web projects using python 3.

The posts must be in  markdown language and are rendered  by Simple Blog to html using python-markdown and codehilite.

It is in very early development and if you wanna contribute,  clone the project and pull back with improvements.

Any doubt contact me at wymored at outlook dot com

Code can be obtained at <https://bitbucket.org/hipercenter/simple-blog>
    """)

@route('/contact')
def contact():
    return template('base.html', globals(), pagetitle='Contact', basetext="""
#Contact
Any doubt about Simple Blog   contact me at
hipercenter at gmail dot com""")

@route('/login')
def login():
    return template('login.html', globals(), pagetitle='Login')

@route('/login', method='POST')
def do_login():
    email = request.forms.get('email')
    password = request.forms.get('password')

    db.execute("select name,email, passw from user where email = ?", (email,))

    row = db.fetchone()
    if row:
        #print(row['passw'])
        if bcrypt.verify(password, row['passw']):
            response.set_cookie("user", row['name'], secret=secret_key_cookie)
            redirect('/manage')
    message = "Password or egetmail don't match. Try again or click on 'Forgot password?' link"
    return template('login.html', globals(), locals(), pagetitle='Login')


@route('/static/<filename:path>')
def send_static(filename):
    return static_file(filename, root='./static')

@route('/favicon.ico')
def send_static(filename='favicon.ico'):
    return static_file(filename, root='./static/images')


@route('/')
def index():
    pagetitle = "Home"
    section = "Blog"

    db.execute('Select * from post where active = 1')
    rows = db.fetchall()


    return template('index.html',globals(),locals())


@route('/blog/<name>')
def article(name):

    db.execute("Select * from post where slug='%s' limit 1" % (name))
    row = db.fetchone()
    #for r in request:
    #    print(r)
    #print(request['HTTP_HOST'])
    return template('post.html', globals(),locals())

@route('/insert')
def insert(name=None):
    username = request.get_cookie("user", secret=secret_key_cookie)
    if username:
        return template('insert.html', globals(),locals(),pagetitle="Insert Article")

    else:
        redirect("/login")
@route('/insert', method='POST')
def do_insert():
    username = request.get_cookie("user", secret=secret_key_cookie)
    if username:
        title = request.forms.get('title')
        content = request.forms.get('content')

        import markdown
        htmlc = markdown.markdown(content, extensions=['markdown.extensions.codehilite'])

        db.execute("""INSERT INTO post (title, slug, content, html_content posted, active) VALUES
        (?,?,?,?,?,1 )""", (title, slugify(title), content, htmlc,datetime.datetime.today().isoformat()))
        con.commit()
        redirect("/manage")
    else:
       redirect("/login")

@route('/edit/<name>')
def edit(name=None):
    username = request.get_cookie("user", secret=secret_key_cookie)
    if username:
        db.execute("SELECT * FROM post WHERE slug='%s'" % (name))
        row = db.fetchone()
        return template('edit.html',globals(),locals(),pagetitle="Edit Post")

    else:
        redirect("/login")

@route('/delete/<name>')
def delete(name=None):
    username = request.get_cookie("user", secret=secret_key_cookie)
    if username:
        print(name)
        db.execute("DELETE FROM post WHERE slug=? ", (name,))
        con.commit()
        redirect("/manage")
    else:
        redirect("/login")


@route('/edit/<name>', method='POST')
def do_update(name):
    username = request.get_cookie("user", secret=secret_key_cookie)
    if username:
        title = request.forms.get('title')
        content = request.forms.get('content')
        active = 1 if  request.forms.get('active') == 'on' else 0
        id = request.forms.get('id')
        import datetime
        from slugify import slugify
        import markdown
        htmlc = markdown.markdown(content, extensions=['markdown.extensions.codehilite'])
        db.execute("""UPDATE post
            SET  title=?, slug=?, content=?, html_content = ?, active=? where id = ? """ , (
            title, slugify(title), content, htmlc,active, id))

        redirect("/manage")

    else:
       redirect("/login")

@route('/manage')
def manage_posts():
    username = request.get_cookie("user", secret=secret_key_cookie)
    if username:
        db.execute("""Select * from post""")
        rows = db.fetchall()

        return template('manage.html', globals(), locals(), pagetitle="Manage")
    else:
        redirect("/login")








if __name__ == '__main__':
    if len(sys.argv) == 5 and sys.argv[1] == 'change_admin':
        change_admin(sys.argv[2],sys.argv[3],sys.argv[4])
    port = int(os.environ.get('PORT', 8080))
    run(host='0.0.0.0', port=port)




