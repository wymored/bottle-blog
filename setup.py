from distutils.core import setup

setup(
    name='bottle-blog',
    version='0.1',
    packages=[''],
    url='',
    license='Domain Public',
    author='wymored',
    author_email='wymored@outlook.com',
    description='a simple blog using Bottle.py',
    install_requires=[
       'unidecode',
    ]
)
