# Bottle Blog

This package was developed to be a base to my new projects using python3. I loved to use web2py, but it still sticks
to python2. Besides, I also want something very simple to use in new projects, and no overbloated as django or web2py
 itself.

# requirements
 python3 (not tested on python2)  
 python-markdown  
 bottle.py  
 python-slugify  
 passlib  
 bcrypt  

## What it have

 A simple complete blog, that uses a sqlite database, and:  
 * Insert and manage articles using markdown. You input markdown and its shows html  
 * Manager area with login
 
## how to use it
Just clone the repository or unpack the zip file.

run the app 

    :::bash
    $ ./blog.py


 
## how modify it
 Read the bottle.py documentation. 
 <http://bottlepy.org/docs/dev/index.html>

 But basically create new functions on blog.py, route
 them and create a new template in views/ to show it.
 
 You can change the header.html, footer.html and other views to your own needs. 
 They are only simple examples to show how bottle.py templates work. 
 
 

